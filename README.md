<?php

namespace TrackingApi;

class TrackingApi
{
    /**
     * @var
     */
    private $url;
    private $type;

    /**
     * @shipmentTypeConstants
     */
    const USPS = 'usps';
    const UPS = 'ups';
    const FEDEX = 'fedex';
    const DHL = 'dhl';


    public function trackingHandler($number, $isAutoTrack = true, $type = '')
    {
        if ($isAutoTrack) {
            $this->type = $this->dataRetrieveHandler("http://shipit-api.herokuapp.com/api/guess/{$number}")[0];

        } else {
            $this->type = $type;
        }
        $this->url = "http://shipit-api.herokuapp.com/api/carriers/{$this->type}/{$number}";
        return $this->dataRetrieveHandler($this->url);

    }

    private function dataRetrieveHandler($url)
    {

        $data = @file_get_contents($url);
        if ($data === false) {
            return false;
        }
        $result = json_decode($data, true);

        return $result;
    }

}


$obj = new TrackingApi();

$data = $obj->trackingHandler('1Z0918WV0306763470');

//foreach ($test['activities'] as $item) {
//  //  print_r($item);
//    echo $item['location'] . '<br>';
//    echo $item['timestamp'] . '<br>';
//    echo '<hr>';
//}
